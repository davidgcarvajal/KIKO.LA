jQuery(document).ready(function($) {
  headerMenu();
  $(".drawer").drawer();
  $('.pick-date').each(function(index, el) {
    $('input',this).pickadate({
      min: new Date()
    });
  });
  $('.pick-time').each(function(index, el) {
    $('input',this).pickatime({
      min: [7,00],
      max: [22,0],
      interval: 15,
    });
  });
  $('#masterslider').each(function(index, el) {
    if ($('.ms-slide img', el).length) {
      $(this).masterslider({
          width: 1000,
          height: 609,
          // more options...
          controls : {
              arrows : {autohide:false},
              bullets : {}
              // more slider controls...
          }
      });
    };
  });
  $('a#back').click(function(){
    window.history.back();
    return false;
  });
  checkiInForm();
  parentHeight('.headerLeft');
  setTimeout(chocolate, 500);
  setTimeout(vCenter, 500);
  window.onresize = function(event) {
    chocolate();
  }
});
function vCenter(){
  //$('a#back').center();
}

function chocolate(){
  $('.chocolate').each(function(){
    $(this).height('auto');
  });
  $('.chocolate').equalHeights()
}
function headerMenu(){
  $('#topmenu-call').click(function(event) {
    var father = $(this).parent();
    $('.top-menu', father).toggleClass('d-hide');
    return false;
  });
}
function checkiInForm(){
  //Handling Submit
  $('#checkin-form').submit(function(event) {
    event.preventDefault();
    // get the form data
    var formData = {
      'people'          : $('select[name=checkin-people]').val(),
      'date'            : $('input[name=checkin-date]').val(),
      'cTime'           : $('input[name=checkin-time]').val()
    };
    $('.active-reservation .guest h5').text(formData.people+ ' People');
    $('.active-reservation .date h5').text(formData.date);
    $('.active-reservation .time h5').text(formData.cTime);
    $.jStorage.set("checkIn",formData);
    $('.complete-reservation-wrapper').show()
    $('.checkin-form-wrapper').hide();;
  });
  //Handling already checkin
  var checkIn_data = $.jStorage.get("checkIn");
  var today = new Date();
  date_tocompare = new Date(checkIn_data.date);
  if (checkIn_data && date_tocompare.getTime() >=  today.getTime() ) {
    $('select[name=checkin-people]').val(checkIn_data.people)
    $('input[name=checkin-date]').val(checkIn_data.date);
    $('input[name=checkin-time]').val(checkIn_data.cTime);
  };
  var csrftoken = $.cookie('csrftoken');
  $('#registration-form').submit(function(event) {
    event.preventDefault();
    $.isLoading({ text: "Loading" });
    if(!validatePass()){
      alert('Error con el pass');
      return false;
    }
    // get the form data
    var formData = {
      'fname'           : $('input[name=fName]').val(),
      'lname'           : $('input[name=lName]').val(),
      'email'           : $('input[name=email]').val(),
      'pass'            : $('input[name=password]').val(),
      'rPass'           : $('input[name=rPassword]').val(),
      'sRequest'         : $('textarea[name=sRequest]').val(),
      'people'          : $('select[name=checkin-people]').val(),
      'date'            : $('input[name=checkin-date]').val(),
      'cTime'           : $('input[name=checkin-time]').val(),
    };
    var action = $('#registration-form').attr('action');
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });    
    $.ajax({
      type: "POST",
      url: action,
      data: formData,
      success: function(response) {
        if (response == "username_error") {
          alert("That email address is already taken please choose another one");
        }
        else{
          $('.complete-reservation-wrapper').hide();
          $('#reservationDone').show();
          $.isLoading("hide");
        }
      },
      error : function(xhr,errmsg,err) {
        // Show an error
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
      }
    });
  });
  $('#d-login-modal').submit(function(event) {
    event.preventDefault();
    $.isLoading({ text: "Loading" });

    // get the form data
    var formData = {
      'email'           : $('input[name=emailLogin]').val(),
      'pass'            : $('input[name=passwordLogin]').val(),
      'sRequest'         : $('textarea[name=sRequest]').val(),
      'people'          : $('select[name=checkin-people]').val(),
      'date'            : $('input[name=checkin-date]').val(),
      'cTime'           : $('input[name=checkin-time]').val(),
    };
    var action = $('#d-login-modal').attr('action');
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });
    $.ajax({
      type: "POST",
      url: action,
      data: formData,
      success: function(response) {
        if (response =="uservalidactiveauthenticated") {
          $('#d-login-modalbox').modal('hide')
          $('.complete-reservation-wrapper').hide();
          $('#reservationDone').show();
          $.isLoading("hide");
        }
        else{
          alert("There is a problem with the checkin, please try again")
        }
      },
      error : function(xhr,errmsg,err) {
        // Show an error
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
      }
    });     
  });
}

$.fn.center = function () {
  var thisHeight = this.outerHeight(true);
  var parentHeight = this.parent().outerHeight(true);
  var newTop = (parentHeight-thisHeight)/6;
  this.css("position","absolute");
  this.parent().css("position","relative");
  this.css("top",newTop);
  this.css("left",'0');
  return this;
}

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function validatePass() {
  var password1 = $("#id_password").val();
  var password2 = $("#id_rPassword").val();
  if(password1 == password2) {
    return true;
  }
  return false;
}

function parentHeight(selector){
  var fatherHeight = $(selector).parent().outerHeight(true);
  $(selector).height(fatherHeight);
;}
/*!
 * Simple jQuery Equal Heights
 *
 * Copyright (c) 2013 Matt Banks
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 *
 * @version 1.5.1
 */
!function(a){a.fn.equalHeights=function(){var b=0,c=a(this);return c.each(function(){var c=a(this).innerHeight();c>b&&(b=c)}),c.css("height",b)},a("[data-equal]").each(function(){var b=a(this),c=b.data("equal");b.find(c).equalHeights()})}(jQuery);