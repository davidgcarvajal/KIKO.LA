from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    topic = models.CharField(max_length=255)
    message = models.TextField(blank=True)

    def __unicode__(self):
        return self.email+": "+self.topic
