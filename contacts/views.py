from django.core.mail import send_mail
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string

from .models import Contact
from .forms import ContactForm

def get_contact(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ContactForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            topic = form.cleaned_data['topic']
            message = form.cleaned_data['message']
            newContact = Contact(
                name=name,
                email=email,
                topic=topic,
                message=message,
            )
            newContact.save()
            body = render_to_string('email_contact.html', {'Contact': newContact})
            sendEmail = send_mail(
                'Nuevo contacto en Kikola',
                'body',
                'info@davidcarvajal.me',
                ["info@davidcarvajal.me", "info@kikokairuz.com"],
                fail_silently=False,
                html_message=body,
            )
            return HttpResponseRedirect('/contact/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactForm()

    return render(request, 'contact.html', {'form': form})


def thankyou(request):
        return render(request,'thankyou.html')
