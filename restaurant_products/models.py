from django.db import models

from restaurants.models import Restaurant
from restaurant_product_types.models import RestaurantProductTypes

class RestaurantProducts(models.Model):
  name = models.CharField(max_length=255)
  machineName = models.CharField(max_length=255)
  image = models.ImageField(upload_to='restaurant_products')
  restaurant = models.ForeignKey(Restaurant)
  product_type = models.ForeignKey(RestaurantProductTypes)
  ingredients = models.TextField(blank=True)

  def __unicode__(self):
    return self.name
