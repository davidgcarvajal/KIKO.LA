# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant_product_types', '0001_initial'),
        ('restaurants', '0003_restaurant_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantProducts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'restaurant_products')),
                ('ingredients', models.TextField(blank=True)),
                ('product_type', models.ForeignKey(to='restaurant_product_types.RestaurantProductTypes')),
                ('restaurant', models.ForeignKey(to='restaurants.Restaurant')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
