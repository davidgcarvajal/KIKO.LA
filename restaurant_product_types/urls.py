from django.conf.urls import patterns, url

from restaurant_product_types import views

urlpatterns = patterns('',
  url(r'^(?P<name>[-\w]+)/$', views.restaurantTypesView),
  url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.restaurantTypeDetailListView),
)
