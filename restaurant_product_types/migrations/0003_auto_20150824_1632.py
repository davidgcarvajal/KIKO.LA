# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant_product_types', '0002_restaurantproducttypes_machinename'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurantproducttypes',
            name='machineName',
            field=models.CharField(unique=True, max_length=255),
            preserve_default=True,
        ),
    ]
