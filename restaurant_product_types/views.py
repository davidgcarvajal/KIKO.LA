from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404

from .models import RestaurantProductTypes
from cities.models import City
from restaurant_products.models import RestaurantProducts

def restaurantTypesView(request, name=0):
  type_list = RestaurantProductTypes.objects.all()
  return render(request, '_restaurant_types.html', {'restaurants_types': type_list})


def restaurantTypeDetailListView(request, name, detail):
  try:
    city = City.objects.get(machineName=name)
    restaurantProductType = RestaurantProductTypes.objects.get(machineName=detail)
    typeProducts = RestaurantProducts.objects.filter(product_type=restaurantProductType.id)
    typeProductsCity = list()
    for products in typeProducts:
      if products.restaurant.city.id == city.id:
        typeProductsCity.append(products)
    typeProductsData = {'type': restaurantProductType, 'ProductType': typeProductsCity}
  except RestaurantProducts.DoesNotExist:
      raise Http404

  return render(request, 'restaurant_type_detail.html', {'ProductTypeData': typeProductsData})
