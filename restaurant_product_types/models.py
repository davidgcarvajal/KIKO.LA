from django.db import models

class RestaurantProductTypes(models.Model):
  name = models.CharField(max_length=255)
  machineName = models.CharField(max_length=255, unique= True)
  image = models.ImageField(upload_to='restaurant_product_types')

  def __unicode__(self):
    return self.name
