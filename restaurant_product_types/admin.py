from django.contrib import admin

from .models import RestaurantProductTypes

admin.site.register(RestaurantProductTypes)
