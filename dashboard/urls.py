from django.conf.urls import patterns, url

from dashboard import views

urlpatterns = patterns(
    '',
    url(r'^$', views.dashboardView),
    url(r'^login/', views.dashboardLogin),
    url(r'^signup/', views.dashboardSignup),
)
