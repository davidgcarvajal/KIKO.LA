# encoding:utf-8
from django import forms


class dashboardLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email'}), max_length=100)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
