from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import dashboardLoginForm


def dashboardView(request):
    dashboardData = {}
    if request.user.is_authenticated():
        dashboardData['user'] = request.user
    else:
        return redirect('/dashboard/login/')

    return render(request, 'dashboard.html', {'dashboardData': dashboardData})


def dashboardLogin(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/dashboard/')
            else:
                loginData = 'Error, cuenta inactiva'
        else:
            loginData = 'Error, credenciales erroneos'
    else:
        loginData = dashboardLoginForm()

    return render(request, 'dashboard_login.html', {'loginData': loginData})


def dashboardSignup(request):
    signupData = "formsignup"
    return render(request, 'dashboard_signup.html', {'signupData': signupData})
