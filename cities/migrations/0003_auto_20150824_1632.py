# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0002_city_machinename'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='machineName',
            field=models.CharField(unique=True, max_length=255),
            preserve_default=True,
        ),
    ]
