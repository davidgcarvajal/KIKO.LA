# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hangout', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hangout',
            old_name='reservationBanner',
            new_name='bookingBanner',
        ),
        migrations.RenameField(
            model_name='hangout',
            old_name='booking',
            new_name='bookingUrl',
        ),
    ]
