from django.db import models
from pyuploadcare.dj import ImageGroupField

from cities.models import City


class Hangout(models.Model):
    name = models.CharField(max_length=255)
    machineName = models.CharField(max_length=255, unique=True)
    image = models.ImageField(upload_to='hangout')
    city = models.ForeignKey(City)
    bookingBanner = models.ImageField(upload_to='hangout_reservation')
    gallery = ImageGroupField()
    bookingUrl = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name
