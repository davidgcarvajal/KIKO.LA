from django.conf.urls import patterns, url

from hangout import views

urlpatterns = patterns(
    '',
    url(r'^$', views.restaurantListView),
    url(r'^(?P<name>[-\w]+)/$', views.restaurantListView),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.restaurantDetail),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/(?P<action>[-\w]+)/$', views.restaurantAction),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/(?P<action>[-\w]+)/(?P<response>[-\w]+)/$', views.restaurantResponse),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/(?P<action>[-\w]+)/(?P<response>[-\w]+)/(?P<status>[-\w]+)/$',
        views.restaurantResponseStatus),
)
