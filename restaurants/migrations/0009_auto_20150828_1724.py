# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0008_restaurantcheckin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurantcheckin',
            name='date',
            field=models.DateField(max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='restaurantcheckin',
            name='people',
            field=models.CharField(max_length=2, choices=[(b'1', b'1 Person'), (b'2', b'2 People'), (b'3', b'3 People'), (b'4', b'4 People'), (b'5', b'5 People'), (b'6', b'6 People'), (b'7', b'7 People'), (b'8', b'8 People'), (b'9', b'9 People'), (b'10', b'10 People'), (b'0', b'More People')]),
            preserve_default=True,
        ),
    ]
