# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0003_restaurant_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='machineName',
            field=models.CharField(default=0, max_length=255),
            preserve_default=False,
        ),
    ]
