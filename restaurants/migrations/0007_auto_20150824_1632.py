# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0006_auto_20150823_1205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='machineName',
            field=models.CharField(unique=True, max_length=255),
            preserve_default=True,
        ),
    ]
