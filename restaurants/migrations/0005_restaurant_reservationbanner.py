# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0004_restaurant_machinename'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='reservationBanner',
            field=models.ImageField(default=0, upload_to=b'restaurants'),
            preserve_default=False,
        ),
    ]
