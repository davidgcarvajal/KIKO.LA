# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0022_auto_20151001_1032'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantManager',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('name', models.CharField(max_length=254)),
                ('cellphone', models.CharField(default=None, max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='managers',
            field=models.ForeignKey(default=None, blank=True, to='restaurants.RestaurantManager', null=True),
            preserve_default=True,
        ),
    ]
