# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0014_restaurantcheckin'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restaurantcheckin',
            name='restaurant',
        ),
        migrations.RemoveField(
            model_name='restaurantcheckin',
            name='user',
        ),
        migrations.DeleteModel(
            name='RestaurantCheckin',
        ),
    ]
