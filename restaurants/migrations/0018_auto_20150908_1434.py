# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0017_restaurant_maingallery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='mainGallery',
            field=pyuploadcare.dj.models.ImageGroupField(),
            preserve_default=True,
        ),
    ]
