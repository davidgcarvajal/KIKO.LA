# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0005_restaurant_reservationbanner'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantGallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(null=True, upload_to=b'gallery', blank=True)),
                ('description', models.TextField(default=None)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='restaurant',
            name='gallery',
            field=models.ManyToManyField(to='restaurants.RestaurantGallery'),
            preserve_default=True,
        ),
    ]
