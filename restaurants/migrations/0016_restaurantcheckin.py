# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('restaurants', '0015_auto_20150904_1135'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantCheckin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submitDate', models.DateTimeField()),
                ('checkinDate', models.DateTimeField()),
                ('people', models.CharField(max_length=2, choices=[(b'1', b'1 Person'), (b'2', b'2 People'), (b'3', b'3 People'), (b'4', b'4 People'), (b'5', b'5 People'), (b'6', b'6 People'), (b'7', b'7 People'), (b'8', b'8 People'), (b'9', b'9 People'), (b'10', b'10 People'), (b'0', b'More People')])),
                ('request', models.TextField(blank=True)),
                ('restaurant', models.ForeignKey(to='restaurants.Restaurant')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
