# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0016_restaurantcheckin'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='mainGallery',
            field=pyuploadcare.dj.models.ImageField(default=''),
            preserve_default=False,
        ),
    ]
