# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0011_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restaurantcheckin',
            name='time',
        ),
        migrations.AlterField(
            model_name='restaurantcheckin',
            name='checkinDate',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
