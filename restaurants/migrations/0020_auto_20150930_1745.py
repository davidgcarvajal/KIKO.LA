# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0019_auto_20150921_0841'),
    ]

    operations = [
        migrations.CreateModel(
            name='RestaurantCheckinStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='restaurantcheckin',
            name='status',
            field=models.ForeignKey(default=None, blank=True, to='restaurants.RestaurantCheckinStatus', null=True),
            preserve_default=True,
        ),
    ]
