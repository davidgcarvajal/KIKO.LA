# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('restaurants', '0009_auto_20150828_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurantcheckin',
            name='checkinDate',
            field=models.DateField(default=datetime.datetime(2015, 9, 4, 15, 46, 31, 376686, tzinfo=utc), max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='restaurantcheckin',
            name='request',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='restaurantcheckin',
            name='restaurant',
            field=models.ForeignKey(default=1, to='restaurants.Restaurant'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='restaurantcheckin',
            name='user',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
