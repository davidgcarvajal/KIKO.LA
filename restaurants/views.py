from django.core.mail import send_mail
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Group
from django.http import HttpResponse, Http404
from django.shortcuts import render
from datetime import datetime
from django.template.loader import render_to_string

from .models import Restaurant, RestaurantCheckinStatus, RestaurantCheckin
from .models import City
from restaurant_products.models import RestaurantProducts
from .forms import CheckinForm, CheckinLogin

import json
import requests

def restaurantListView(request, name=0):
    if name != 0:
        try:
            city = City.objects.get(machineName=name)
            restaurants_list = Restaurant.objects.filter(city=city.id)
        except Restaurant.DoesNotExist:
            raise Http404
    else:
        restaurants_list = Restaurant.objects.all()

    return render(request, 'restaurant_list.html', {'restaurants_list': restaurants_list})


def restaurantDetail(request, name, detail):
    try:
        restaurant = Restaurant.objects.get(machineName=detail)
        restaurantProducts = RestaurantProducts.objects.filter(restaurant=restaurant.id)
        restaurant_data = {'restaurant': restaurant, 'products': restaurantProducts}
    except Restaurant.DoesNotExist:
        raise Http404

    return render(request, 'restaurant.html', {'restaurantData': restaurant_data})


def restaurantAction(request, name, detail, action):
    try:
        restaurant = Restaurant.objects.get(machineName=detail)
        form = CheckinForm()
        loginForm = CheckinLogin()
        restaurant_data = {
            'restaurant': restaurant,
            'name': name,
            'detail': detail,
            'action': action,
            'form': form,
            'loginForm': loginForm,
        }
    except Restaurant.DoesNotExist:
        raise Http404
    return render(request, 'checkin.html', {'restaurantData': restaurant_data})


def restaurantResponse(request, name, detail, action, response):
    if response == "response" and request.POST:
        # User data
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        # TODO sanity username
        email = request.POST.get('email')
        uPass = request.POST.get('pass')
        username = email
        if User.objects.filter(username=username).exists():
            return HttpResponse(
                json.dumps('username_error'),
                content_type="application/json"
            )
        # User create
        user = User.objects.create_user(username, email, uPass)
        user.first_name = fname
        user.last_name = lname
        user.save()
        user.groups.add(Group.objects.get(name='client'))

        # Checkin data
        emailResponse = processCheckin(request, detail, user)

        # TODO manage errors
        return HttpResponse(
            json.dumps(emailResponse),
            content_type="application/json"
        )

    elif response == "responseLogin" and request.POST:
        email = request.POST.get('email')
        uPass = request.POST.get('pass')
        user = authenticate(username=email, password=uPass)
        if user is not None:
            # the password verified for the user
            if user.is_active:
                # Checkin data
                processCheckin(request, detail, user)
                message = "uservalidactiveauthenticated"
            else:
                message = "passwordvalidaccountdisabled!"
        else:
            # the authentication system was unable to verify the username and password
            message = "usernamepasswordincorrect."

        return HttpResponse(
            json.dumps(message),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"nothing to see": request.method}),
            content_type="application/json"
        )


def restaurantResponseStatus(request, name, detail, action, response, status):
    try:
        restaurant = Restaurant.objects.get(machineName=detail)
        checkin = RestaurantCheckin.objects.get(id=status)
        restaurant_data = {
            'Restaurant': restaurant,
            'name': name,
            'detail': detail,
            'action': action,
            'response': response,
            'Checkin': checkin,
        }
        State = RestaurantCheckinStatus.objects.get(name=response)
        checkin.status = State
        checkin.save()
        body = render_to_string('email_user.html', {'checkin': checkin})
        dSendEmail("checkin.user.email", 'KIKO.LA - checkin '+response, body)

    except RestaurantCheckin.DoesNotExist:
        raise Http404
    return render(request, 'response.html', {'restaurantData': restaurant_data})


def processCheckin(request, restaurantName, user):
    people = request.POST.get('people')
    date = request.POST.get('date')
    cTime = request.POST.get('cTime')
    dateString = date+" "+cTime
    dateFormated = datetime.strptime(dateString, "%d %B, %Y %I:%M %p")
    sRequest = request.POST.get('sRequest')
    restaurant = Restaurant.objects.get(machineName=restaurantName)
    status = RestaurantCheckinStatus.objects.get(name='pending')
    # Checkin create
    Checkin = RestaurantCheckin(
        user=user,
        restaurant=restaurant,
        submitDate=datetime.now(),
        checkinDate=dateFormated,
        people=people,
        request=sRequest,
        status=status,
    )
    Checkin.save()
    # Restaurant confirmation email
    body = render_to_string('email_restaurant.html', {'checkin': Checkin})
    emailResponse = dSendEmail(restaurant.managers.email, 'Nuevo checkin en Kiko.la', body)
    return "uservalidactiveauthenticated"


def dSendEmail(to, subject, body):
    return requests.post(
        "https://api.mailgun.net/v3/kiko.la/messages",
        auth=("api", "key-0810c9fcdb29da53791de52e03b50b5e"),
        data={"from": "Kiko.la <info@kiko.la>",
                "to": [to],
                "subject": subject,
                "text": 'Your mail do not support HTML',
                'html': body,
                "o:tag": ["Checkin", "notifications"]
        })
