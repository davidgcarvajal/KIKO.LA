from django.contrib import admin

from .models import Restaurant, RestaurantCheckinStatus, RestaurantCheckin, RestaurantManager

admin.site.register(Restaurant)
admin.site.register(RestaurantCheckinStatus)
admin.site.register(RestaurantCheckin)
admin.site.register(RestaurantManager)
