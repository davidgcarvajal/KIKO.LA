from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj import ImageGroupField

from cities.models import City


PAX_CHOISES = (
    ('1', '1 Person'),
    ('2', '2 People'),
    ('3', '3 People'),
    ('4', '4 People'),
    ('5', '5 People'),
    ('6', '6 People'),
    ('7', '7 People'),
    ('8', '8 People'),
    ('9', '9 People'),
    ('10', '10 People'),
    ('0', 'More People')
)

class RestaurantManager(models.Model):
    email = models.EmailField(max_length=254)
    name = models.CharField(max_length=254)
    cellphone = models.CharField(max_length=255, null=True, blank=True, default=None)

    def __unicode__(self):
        return self.name


class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    machineName = models.CharField(max_length=255, unique=True)
    image = models.ImageField(upload_to='restaurants')
    city = models.ForeignKey(City)
    reservationBanner = models.ImageField(upload_to='restaurants')
    mainGallery = ImageGroupField()
    location = models.TextField(max_length=500)
    managers = models.ForeignKey(RestaurantManager, null=True, blank=True, default=None)

    def __unicode__(self):
        return self.name


class RestaurantCheckinStatus(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class RestaurantCheckin(models.Model):
    user = models.ForeignKey(User)
    restaurant = models.ForeignKey(Restaurant)
    submitDate = models.DateTimeField()
    checkinDate = models.DateTimeField()
    people = models.CharField(max_length=2, choices=PAX_CHOISES)
    request = models.TextField(blank=True)
    status = models.ForeignKey(RestaurantCheckinStatus, null=True, blank=True, default=None)

    def __unicode__(self):
        returningString = self.restaurant.name+"_people:"+self.people
        return returningString
