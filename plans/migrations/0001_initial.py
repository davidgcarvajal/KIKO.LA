# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0003_auto_20150824_1632'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('machineName', models.CharField(unique=True, max_length=255)),
                ('image', models.ImageField(upload_to=b'plan')),
                ('reservationBanner', models.ImageField(upload_to=b'plan')),
                ('mainGallery', pyuploadcare.dj.models.ImageGroupField()),
                ('location', models.TextField(max_length=500)),
                ('city', models.ForeignKey(to='cities.City')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlanCheckin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submitDate', models.DateTimeField()),
                ('checkinDate', models.DateTimeField()),
                ('people', models.CharField(max_length=2, choices=[(b'1', b'1 Person'), (b'2', b'2 People'), (b'3', b'3 People'), (b'4', b'4 People'), (b'5', b'5 People'), (b'6', b'6 People'), (b'7', b'7 People'), (b'8', b'8 People'), (b'9', b'9 People'), (b'10', b'10 People'), (b'0', b'More People')])),
                ('request', models.TextField(blank=True)),
                ('plan', models.ForeignKey(to='plans.Plan')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlanCheckinStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='plancheckin',
            name='status',
            field=models.ForeignKey(default=None, blank=True, to='plans.PlanCheckinStatus', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plancheckin',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
