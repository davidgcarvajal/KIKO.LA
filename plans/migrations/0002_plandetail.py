# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plans', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlanDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('machineName', models.CharField(unique=True, max_length=255)),
                ('image', models.ImageField(upload_to=b'plandetail')),
                ('description', models.TextField(max_length=500)),
                ('plan', models.ForeignKey(to='plans.Plan')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
