from django.shortcuts import render

from .models import Plan
from .models import City
from .forms import CheckinForm, CheckinLogin

def planDetail(request, name, detail):
    try:
        plan = Plan.objects.get(machineName=detail)
        city = City.objects.get(machineName=name)
        plan_data = {'plan': plan, 'city': city}
    except Plan.DoesNotExist:
        raise Http404

    return render(request, 'plan.html', {'planData': plan_data})


def planAction(request, name, detail, action):
    try:
        plan = Plan.objects.get(machineName=detail)
        form = CheckinForm()
        loginForm = CheckinLogin()
        plan_data = {
            'plan': plan,
            'name': name,
            'detail': detail,
            'action': action,
            'form': form,
            'loginForm': loginForm,
        }
    except Plan.DoesNotExist:
        raise Http404
    return render(request, 'checkin.html', {'planData': plan_data})