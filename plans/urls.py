from django.conf.urls import patterns, url

from plans import views

urlpatterns = patterns(
    '',
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.planDetail),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/(?P<action>[-\w]+)/$', views.planAction),
)