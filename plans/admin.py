from django.contrib import admin

from .models import Plan, PlanCheckinStatus, PlanCheckin

admin.site.register(Plan)
admin.site.register(PlanCheckinStatus)
admin.site.register(PlanCheckin)

