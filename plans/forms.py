# encoding:utf-8
from django import forms


class CheckinForm(forms.Form):
    people = forms.CharField(max_length=100)
    date = forms.CharField(max_length=100)
    time = forms.CharField(max_length=100)
    fName = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First Name'}), max_length=100)
    lName = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last Name'}), max_length=100)
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email'}), max_length=100)
    sRequest = forms.CharField(
        label='Request', widget=forms.Textarea(attrs={'placeholder': 'Add a special request (optional)', 'rows': 3}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    rPassword = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Repeat Password'}))


class CheckinLogin(forms.Form):
    peopleLogin = forms.CharField(max_length=100)
    dateLogin = forms.CharField(max_length=100)
    timeLogin = forms.CharField(max_length=100)
    emailLogin = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email'}), max_length=100)
    sRequestLogin = forms.CharField(
        label='Request', widget=forms.Textarea(attrs={'placeholder': 'Add a special request (optional)', 'rows': 3}))
    passwordLogin = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))