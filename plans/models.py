from django.db import models
from django.contrib.auth.models import User
from pyuploadcare.dj import ImageGroupField

from cities.models import City

PAX_CHOISES = (
    ('1', '1 Person'),
    ('2', '2 People'),
    ('3', '3 People'),
    ('4', '4 People'),
    ('5', '5 People'),
    ('6', '6 People'),
    ('7', '7 People'),
    ('8', '8 People'),
    ('9', '9 People'),
    ('10', '10 People'),
    ('0', 'More People')
)

class Plan(models.Model):
    name = models.CharField(max_length=255)
    machineName = models.CharField(max_length=255, unique=True)
    image = models.ImageField(upload_to='plan')
    city = models.ForeignKey(City)
    reservationBanner = models.ImageField(upload_to='plan')
    mainGallery = ImageGroupField()
    location = models.TextField(max_length=500)

    def __unicode__(self):
        return self.name

class PlanCheckinStatus(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class PlanCheckin(models.Model):
    user = models.ForeignKey(User)
    plan = models.ForeignKey(Plan)
    submitDate = models.DateTimeField()
    checkinDate = models.DateTimeField()
    people = models.CharField(max_length=2, choices=PAX_CHOISES)
    request = models.TextField(blank=True)
    status = models.ForeignKey(PlanCheckinStatus, null=True, blank=True, default=None)

    def __unicode__(self):
        returningString = self.plan.name+"_people:"+self.people
        return returningString

class PlanDetail(models.Model):
    plan = models.ForeignKey(Plan)
    name = models.CharField(max_length=255)
    machineName = models.CharField(max_length=255, unique=True)
    image = models.ImageField(upload_to='plandetail')
    description = models.TextField(max_length=500)

    def __unicode__(self):
        return self.name
