#!/bin/bash

NAME="kikola" # Name of the application
DJANGODIR=/home/dcarvajal/django/kikola/ # Django project directory
LOGFILE=/home/dcarvajal/django/kikola/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)
USER=dcarvajal # the user to run as
GROUP=dcarvajal # the group to run as
ADDRESS=107.170.214.139:8000
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=kikola.settings # which settings file should Django use
DJANGO_WSGI_MODULE=kikola.wsgi # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/dcarvajal/django/venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
    --workers $NUM_WORKERS \
    --bind=$ADDRESS \
    --user=$USER --group=$GROUP \
    --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE
