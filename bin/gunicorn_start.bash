#!/bin/bash

NAME="kikola" # Name of the application
DJANGODIR=/home/dcarvajal/django/kikola/ # Django project directory
SOCKFILE=/home/dcarvajal/django/kikola/run/gunicorn.sock
USER=dcarvajal # the user to run as
GROUP=dcarvajal # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=kikola.settings # which settings file should Django use
DJANGO_WSGI_MODULE=kikola.wsgi # WSGI module name

LOGFILE=/home/dcarvajal/django/kikola/logs/gunicorn.log
LOGDIR=$(dirname $LOGFILE)


ADDRESS=107.170.214.139:8000

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/dcarvajal/django/venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
    --name $NAME \
    --workers $NUM_WORKERS \
    --user=$USER --group=$GROUP \
    --bind=unix:$SOCKFILE \
    --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE

