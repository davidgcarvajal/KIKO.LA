from django.shortcuts import render

from .models import Editorial
from .models import EditorialDetail
from .models import City


def editorialDetail(request, name, detail):
	try:
		editorial = Editorial.objects.get(machineName=detail)
		city = City.objects.get(machineName=name)
		editorialDetail = EditorialDetail.objects.filter(editorial=editorial.id)
		data = {
			'detail': editorial,
			'city': city,
			'editorial': editorialDetail,
		}
	except Editorial.DoesNotExist:
		raise Http404

	return render(request, 'editorial.html', {'data': data})
