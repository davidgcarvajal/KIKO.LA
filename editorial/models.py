from django.db import models
from django.contrib.auth.models import User
from cities.models import City
from pyuploadcare.dj import ImageGroupField
from django.core.validators import URLValidator


class Editorial(models.Model):
	name = models.CharField(max_length=255)
	machineName = models.CharField(max_length=255, unique=True)
	slideshow = ImageGroupField()
	city = models.ForeignKey(City)

	def __unicode__(self):
		return self.name


class EditorialDetail(models.Model):
	editorial = models.ForeignKey(Editorial)
	name = models.CharField(max_length=255)
	image = models.ImageField(upload_to='editorialDetail')
	url = models.CharField(max_length=255)

	def __unicode__(self):
		return self.name
