# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('editorial', '0003_remove_editorial_maingallery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='editorialdetail',
            name='url',
            field=models.CharField(max_length=255),
            preserve_default=True,
        ),
    ]
