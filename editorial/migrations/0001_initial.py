# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0003_auto_20150824_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='Editorial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('machineName', models.CharField(unique=True, max_length=255)),
                ('slideshow', pyuploadcare.dj.models.ImageGroupField()),
                ('mainGallery', pyuploadcare.dj.models.ImageGroupField()),
                ('city', models.ForeignKey(to='cities.City')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EditorialDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'editorialDetail')),
                ('url', models.TextField(validators=[django.core.validators.URLValidator()])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
