# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('editorial', '0002_editorialdetail_editorial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='editorial',
            name='mainGallery',
        ),
    ]
