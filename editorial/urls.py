from django.conf.urls import patterns, url

from editorial import views

urlpatterns = patterns(
	'',
	url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.editorialDetail),
)
