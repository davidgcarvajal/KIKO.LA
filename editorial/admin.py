from django.contrib import admin

from .models import Editorial, EditorialDetail

admin.site.register(Editorial)
admin.site.register(EditorialDetail)
