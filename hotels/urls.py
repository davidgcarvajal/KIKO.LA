from django.conf.urls import patterns, url

from hotels import views

urlpatterns = patterns(
    '',
    url(r'^$', views.hotelListView),
    url(r'^(?P<name>[-\w]+)/$', views.hotelListView),
    url(r'^(?P<name>[-\w]+)/(?P<detail>[-\w]+)/$', views.hotelDetail),
)
