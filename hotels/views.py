from django.http import HttpResponse, Http404
from django.shortcuts import render
from .models import Hotel
from .models import City


def hotelListView(request, name=0):
    if name != 0:
        try:
            city = City.objects.get(machineName=name)
            hotel_list = Hotel.objects.filter(city=city.id)
        except Hotel.DoesNotExist:
            raise Http404
    else:
        hotel_list = Hotel.objects.all()

    return render(request, 'hotel_list.html', {'data': hotel_list})


def hotelDetail(request, name, detail):
    try:
        hotel = Hotel.objects.get(machineName=detail)
    except Hotel.DoesNotExist:
        raise Http404

    return render(request, 'hotel.html', {'data': hotel})
