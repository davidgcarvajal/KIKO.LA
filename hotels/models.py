from django.db import models
from pyuploadcare.dj import ImageGroupField
from cities.models import City


class Hotel(models.Model):
    name = models.CharField(max_length=255)
    machineName = models.CharField(max_length=255, unique=True)
    city = models.ForeignKey(City)
    image = models.ImageField(upload_to='hotels')
    mainGallery = ImageGroupField()
    bookingUrl = models.TextField(max_length=512)
    location = models.TextField(max_length=500)

    def __unicode__(self):
        return self.name
