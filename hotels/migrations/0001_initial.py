# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities', '0003_auto_20150824_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('machineName', models.CharField(unique=True, max_length=255)),
                ('image', models.ImageField(upload_to=b'hotels')),
                ('reservationBanner', models.ImageField(upload_to=b'hotels_reservation')),
                ('city', models.ForeignKey(to='cities.City')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HotelGallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(null=True, upload_to=b'hgallery', blank=True)),
                ('description', models.TextField(default=None)),
            ],
            options={
                'ordering': ('name',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='hotel',
            name='gallery',
            field=models.ManyToManyField(to='hotels.HotelGallery'),
            preserve_default=True,
        ),
    ]
