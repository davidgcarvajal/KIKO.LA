# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('hotels', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hotel',
            name='gallery',
        ),
        migrations.DeleteModel(
            name='HotelGallery',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='image',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='reservationBanner',
        ),
        migrations.AddField(
            model_name='hotel',
            name='mainGallery',
            field=pyuploadcare.dj.models.ImageGroupField(default=''),
            preserve_default=False,
        ),
    ]
