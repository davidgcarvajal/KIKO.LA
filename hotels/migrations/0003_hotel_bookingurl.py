# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotels', '0002_auto_20160211_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='bookingUrl',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
