# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotels', '0003_hotel_bookingurl'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='bookingUrl',
            field=models.TextField(max_length=512),
            preserve_default=True,
        ),
    ]
