# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotels', '0005_hotel_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='location',
            field=models.TextField(default='', max_length=500),
            preserve_default=False,
        ),
    ]
